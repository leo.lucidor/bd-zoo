DROP TABLE Remplacement;
DROP TABLE Avoir;
DROP TABLE Aliment;
DROP TABLE Animaux;
DROP TABLE Localiser;
DROP TABLE Especes; 
DROP TABLE Famille;
DROP TABLE Zoologie; 
DROP TABLE Safari; 
DROP TABLE Emplacement;
DROP TABLE Type_emplacement;
DROP TABLE Zone_animaux;
DROP TABLE Zoo;



CREATE TABLE Zoo(
    IDzoo number(4),
    NomZoo varchar2(25),
    VilleZoo varchar2(20),
    PaysZoo varchar2(28),
    TelZoo number(10),
    EmailZoo varchar2(30),
    Nom_responsable varchar2(15), 
    constraint clé_Zoo PRIMARY KEY(IDzoo)     
);


CREATE TABLE Zoologie(
    IDzoo number(4) NOT NULL,
    Intitulé_équipe_de_recherche varchar2(35),
    constraint clé_Zoologie PRIMARY KEY(IDzoo)
);



CREATE TABLE Safari(
    IDzoo number(4) NOT NULL,
    Superficie varchar2(10),
    Mode_de_locomotion varchar2(15),
    constraint clé_Safari PRIMARY KEY(IDzoo)
);


CREATE TABLE Type_emplacement( 
    Code_type_emplacement number(4),
    procedure_de_securite varchar2(100),
    Libelle varchar2(100),
    constraint clé_Type_emplacement PRIMARY KEY(Code_type_emplacement)
);


CREATE TABLE Emplacement(
    Code_emplacement number(4),
    Code_type_emplacement number(4) NOT NULL,
    Situation_emlpacement varchar2(15),
    constraint clé_Emplacement PRIMARY KEY(Code_emplacement)
);


 
CREATE TABLE Remplacement(
    catalogue_aliment varchar2(300) NOT NULL,
    catalogue_aliment2 varchar2(300) NOT NULL,
    aliment_de_substitution varchar2(300), 
    constraint clé_remplacement PRIMARY KEY(Catalogue_aliment)
);
 


CREATE TABLE Avoir(
    IDzoo number(4) NOT NULL,
    Catalogue_aliment varchar2(300) NOT NULL,
    quantite_stock number(4), 
    constraint clé_Avoir PRIMARY KEY(IDzoo, catalogue_aliment)
);



CREATE TABLE Zone_animaux(
    Code_zone number(4) NOT NULL,
    libelle_zone varchar2(100),
    constraint clé_Zone_animaux PRIMARY KEY(Code_zone)
);
 


CREATE TABLE Famille( 
    Code_famille number(4),
    description_famille varchar2(400),
    caractere varchar2(50),
    constraint clé_Famille PRIMARY KEY(Code_famille)
);


CREATE TABLE Especes(
    Code_especes number(4),
    Code_famille number(4) NOT NULL,
    Code_zone number(4) NOT NULL,
    NomE varchar2(25),
    Nom_secteur varchar2(25),
    Nom_vulgaire varchar2(25),
    constraint clé_Especes PRIMARY KEY(Code_especes)
);



CREATE TABLE Animaux(
    Code_animaux number(4),
    Code_especes number(4),
    sexe varchar2(10),
    date_de_naissance varchar2(10),
    date_darriver varchar2(10),
    remarque varchar2(300),
    nom_individue varchar2(30),
    constraint clé_Animaux PRIMARY KEY(Code_animaux)
);



CREATE TABLE Aliment(
    Catalogue_aliment varchar2(300),
    IDzoo number(4) NOT NULL,
    constraint clé_Aliment PRIMARY KEY(Catalogue_aliment)
);


CREATE TABLE Localiser(
    Code_zone number(4) NOT NULL,
    Code_especes number(4) NOT NULL,
    total_effectif number(4),
    constraint clé_Localiser PRIMARY KEY(Code_zone, Code_especes)
);







ALTER TABLE Zoologie ADD constraint fk_Zoologie FOREIGN KEY(IDzoo) REFERENCES Zoo(IDzoo);
ALTER TABLE Safari ADD constraint fk_Safari FOREIGN KEY(IDzoo) REFERENCES Zoo(IDzoo);
ALTER TABLE Emplacement ADD constraint fk_Emplacement FOREIGN KEY(Code_type_emplacement) REFERENCES Type_emplacement(Code_type_emplacement);
ALTER TABLE Remplacement ADD constraint fk_Remplacement FOREIGN KEY(Catalogue_aliment) REFERENCES Aliment(Catalogue_aliment);
ALTER TABLE Avoir ADD constraint fk_Avoir FOREIGN KEY(IDzoo) REFERENCES Zoo(IDzoo);
ALTER TABLE Avoir ADD constraint fk2_Avoir FOREIGN KEY(Catalogue_aliment) REFERENCES Aliment(Catalogue_aliment);
ALTER TABLE Animaux ADD constraint fk_Animaux FOREIGN KEY(Code_especes) REFERENCES Especes(Code_especes);
ALTER TABLE Especes ADD constraint fk_Especes FOREIGN KEY(Code_famille) REFERENCES Famille(Code_famille);
ALTER TABLE Especes ADD constraint fk2_Especes FOREIGN KEY(Code_zone) REFERENCES Zone_animaux(Code_zone);
ALTER TABLE Aliment ADD constraint fk_Aliment FOREIGN KEY(IDzoo) REFERENCES Zoo(IDzoo); 
ALTER TABLE Localiser ADD constraint fk_Localiser FOREIGN KEY(Code_zone) REFERENCES Zone_animaux(Code_zone);
ALTER TABLE Localiser ADD constraint fk2_Localiser FOREIGN KEY(Code_especes) REFERENCES Especes(Code_especes);



