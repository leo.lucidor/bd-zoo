insert into Zoologie values(123, 'exemple intitule de recherche');
insert into Zoologie values(234, 'exemple intitule de recherche 2');
insert into Safari values(123,'100km', 'voiture');
insert into Safari values(234,'200km','Camionette');
insert into Zoo values(123,'beauval','chartre','france',0606060606,'erererererer@gmail.com','lucon');
insert into Zoo values(234,'beau','dreuc','suisse',0707070707,'ersgzgrer@gmail.com','fagon');
insert into Emplacement values(10,11,'C4');
insert into Emplacement values(13,15,'E2');
insert into Type_emplacement values(11,'en cas de feux refigier vous dans le parc','cage');
insert into Type_emplacement values(15,'en cas de feux refigier vous dans la sortie','aquarium');
insert into Remplacement values('poire,pomme,carotte,salade,olive','foin,choux','herbe');
insert into Remplacement values('foin,choux','herbe','poire,pomme,carotte,salade,olive');
insert into Avoir values(123,'poire,pomme,carotte,salade,olive',1256);
insert into Avoir values(234,'foin,choux, poire,pomme', 9665);
insert into Zone_animaux values(12,'libezllezone E10');
insert into Zone_animaux values(16,'libellezone E10');
insert into Famille values(1,'une famille de coleoptere','enervant');
insert into Famille values(2,'une famille de vertebre','libre');
insert into Especes values(2,2,123,'peroquet','dome','oiseau');
insert into Especes values(2,2,123,'Grebe','dome','oiseau');
insert into Especes values(1,1,123,'scarabe','air libre','insecte');
insert into Especes values(1,1,234,'coccinelle','air libre','insecte');
insert into Animaux values(6,2,'male', to_date('10/02/2012', 'DD/MM/YYYY'), to_date('02/03/2011', 'DD/MM/YYYY'), 'aucune remarque','jean-marc');
insert into Animaux values(8,1,'femelle', to_date('10/08/2021','DD/MM/YYYY'),to_date('02/06/2010', 'DD/MM/YYYY'), 'aucune remarque','daniel');
insert into Aliment values('foin,herbe,poire,pomme,carotte,salade,olive,poire,pomme,carotte,salade,olive',123);
insert into Aliment values('foin,choux,herbe,poire,pomme,carotte,salade,choux fleur,olive,carotte,salade,olive',234);
insert into Localiser values(12, 2, 25);
insert into Localiser values(16, 1, 68);





set linesize 300;
select * from Zoo;
select * from Zoologie;
select * from Safari;
select * from Aliment;
select * from Remplacement;
select * from Emplacement;
select * from Type_emplacement;
select * from Animaux;
select * from Especes;
select * from Famille;
select * from Zone_animaux;